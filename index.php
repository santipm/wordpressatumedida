<?php

//Cargar librerias
require_once('header.php');
require_once('menus.php');
require_once('users.php');

// Cargar Cabecera
get_header();

?>
<body>
		<!-- start: Header -->
		<?php 
			get_menu_top();
		?>
		<!-- start: Header -->
	
		<div class="container-fluid-full">
			<div class="row-fluid">
				
				<!-- start: Main Menu -->
				<?php get_main_menu(); ?>
				<!-- end: Main Menu -->
			
				<noscript>
					<div class="alert alert-block span10">
						<h4 class="alert-heading">Warning!</h4>
						<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
					</div>
				</noscript>
			
				<!-- start: Content -->
				<div id="content" class="span10">
					<div class="span12">
					<?php
						$breadcrumbs = array('Panel de control' => 'index.php', 'Otro' =>'perfil.php');
						get_breadcrumbs($breadcrumbs); 
					?>
					</div>
					<div class="span10">
					 <!-- Listar usuarios -->
						<div ondesktop="span10" ontablet="span6" class="box black span4">
						<div class="box-header">
							<h2><i class="halflings-icon white user"></i><span class="break"></span>Last Users</h2>
							<div class="box-icon">
								<a class="btn-minimize" href="#"><i class="halflings-icon white chevron-up"></i></a>
								<a class="btn-close" href="#"><i class="halflings-icon white remove"></i></a>
							</div>
						</div>
						<div class="box-content" style="display: block;">
								<ul class="dashboard-list metro">
									
									<?php
						
										foreach($usuarios as $key => $value):
											echo '<li class="'.get_status($value['estado']).'">';
											echo 	'<a href="#">';
											echo 		'<img src="'.$value['foto'].'" alt="'.$value['nombre'].'" class="avatar">';
											echo 	'</a>';
											echo 	'<strong>Nombre:</strong> '.$value['nombre'].'<br>';
											echo	'<strong>Desde:</strong> '.$value['creado'].'"<br>';
											echo	'<strong>Estado:</strong> '.$value['estado'].'' ;          
											echo '</li>';
										endforeach;
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2015 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Curso Wordpress a tu medida</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

	<?php 
		require('libreria_js.php');
	?>
	
</body>
</html>
