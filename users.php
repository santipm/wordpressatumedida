<?php

$usuario1 = array(
				'nombre' => 'Juan Carlos',
				'creado' => '25 de Octubre',
				'estado' => 'Aprobado',
				'foto'	 => 'img/foto1.jpg'
				);

				
$usuario2 = array(
				'nombre' => 'Josefa',
				'creado' => '20 de Mayo',
				'estado' => 'Aprobado',
				'foto'	 => 'img/foto2.jpg'
				);


$usuario3 = array(
				'nombre' => 'Inma',
				'creado' => '203 de Febrero',
				'estado' => 'Pendiente',
				'foto'	 => 'img/foto3.jpg'
				);
				
$usuario4 = array(
				'nombre' => 'Rosana',
				'creado' => '20 de Octubre',
				'estado' => 'Denegado',
				'foto'	 => 'img/foto4.jpg'
				);		
				
$usuarios = array($usuario1,$usuario2,$usuario3,$usuario4);

function get_status($estado){
	
	switch($estado){
		
		case 'Aprobado':
			$resultado = 'green';
			break;
		case 'Denegado':
			$resultado = 'red';
			break;
		case 'Pendiente':
			$resultado = 'yellow';
			break;
		default:
			$resultado = 'blue';
			break;
	} 
	
	return $resultado;
}

?>		